package com.automatic.api.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.relevantcodes.extentreports.LogStatus;
import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPParams;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.utils.testutils.BaseTest;
import com.rummycitcle.dataProvider.RC_DataProvider;

public class FS_SearchAccount extends Executor{
	
		
	@Test(dataProvider = "getTestDataForMyTest", dataProviderClass = RC_DataProvider.class)
	public void Fun_Account(String data)
	{
		Map<String, String> response = execute(data);
		
		if (!response.isEmpty()) {
			Assert.assertTrue(true);
		}else
			Assert.assertTrue(false);
		
	}

	@Test(dataProvider = "getTestDataForMyTest", dataProviderClass = RC_DataProvider.class)
	public void System_Account(String data)
	{
		Map<String, String> response = execute(data);
		
		if (!response.isEmpty()) {
			Assert.assertTrue(true);
		}else
			Assert.assertTrue(false);
		
	}

	@Test(dataProvider = "getTestDataForMyTest", dataProviderClass = RC_DataProvider.class)
	public void Player_Account(String data)
	{
		Map<String, String> response = execute(data);
		
		if (!response.isEmpty()) {
			Assert.assertTrue(true);
		}else
			Assert.assertTrue(false);
		
	}

	@Test(dataProvider = "getTestDataForMyTest", dataProviderClass = RC_DataProvider.class)
	public void In_Play_Account(String data)
	{
		Map<String, String> response = execute(data);
		
		if (!response.isEmpty()) {
			Assert.assertTrue(true);
		}else
			Assert.assertTrue(false);
		
	}

}
