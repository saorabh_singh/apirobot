package com.rummycircle.automaticapitest;

import testCalassCreation.CreateApiTestClass;
import jsontoCsvConverter.TestDataCreation;

public class GenrateDataAndTests {

	public static void main(String[] args) {

		String jsonfilepath ="/home/saorabh/Documents/test1.json";
		String testDataFileName,testDataFileinfo,sheetName;
		
		testDataFileinfo = TestDataCreation.createTestDataFromJson(jsonfilepath);
		testDataFileName=testDataFileinfo.split("\\|")[0];
		sheetName=testDataFileinfo.split("\\|")[1];

		String[] header = CreateApiTestClass.getTestMethodName(testDataFileName, sheetName);
		header[0]=sheetName;

		CreateApiTestClass.createTestClass("sampleApiTestClass.txt",header);
		CreateApiTestClass.createTestXML("automaticApiTesting.xml",header,testDataFileName);
	}
}
