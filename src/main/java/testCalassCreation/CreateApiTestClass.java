package testCalassCreation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CreateApiTestClass {
	static XSSFSheet ExcelWSheet;
	static XSSFWorkbook ExcelWBook;
	static XSSFCell Cell;


	public static void main(String[] args) {

		String sheetName="FS_SearchTransaction";
		String FilePath="Test_Data_Saorabh.xlsx";
		String[] header = getTestMethodName(FilePath, sheetName);
		header[0]=sheetName;

		createTestClass("sampleApiTestClass.txt",header);
		createTestXML("automaticApiTesting.xml",header,FilePath);
	}



	static public void createTestXML(String sampleFileName, String[] header, String filePath) {

		ArrayList<String> finalList = new ArrayList<String>();
		String line = null;

		try
		{
			URL url = Thread.currentThread().getContextClassLoader().getResource(sampleFileName);
			File f1 = new File(url.getPath());
			File updatedFile = new File(header[0]+".xml");
			FileReader fr = new FileReader(f1);
			BufferedReader br = new BufferedReader(fr);

			while ((line = br.readLine()) != null)
				finalList.add(line);

			FileWriter fw = new FileWriter(updatedFile);
			BufferedWriter out = new BufferedWriter(fw);

			finalList.set(9, finalList.get(9).replace("Default test", header[0]));
			finalList.set(10, finalList.get(10).replace("SheetName", header[0]));
			finalList.set(10, finalList.get(10).replace("FileName", filePath));
			finalList.set(12, finalList.get(12).replace("ClassName", header[0]));

			String temp = finalList.get(14);
			finalList.remove(14);

			for (int i = 1; i < header.length; i++) 
				finalList.add(14,temp.replace("TestFunc", header[i]));

			for (Iterator iterator = finalList.iterator(); iterator.hasNext();) {
				String allLines = (String) iterator.next();
				out.append(allLines);
				out.append("\n");
			}
			out.close();
			System.out.println("TestNG XML Created Successfully : "+updatedFile.getAbsolutePath());
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}


	static public void createTestClass(String sampleClassFileName, String[] header) {

		ArrayList<String> part1 = new ArrayList<String>();
		ArrayList<String> part2 = new ArrayList<String>();
		ArrayList<String> part3 = new ArrayList<String>();
		ArrayList<String> finalList = new ArrayList<String>();
		String line = null;

		try
		{
			URL url = Thread.currentThread().getContextClassLoader().getResource(sampleClassFileName);
			File f1 = new File(url.getPath());
			File updatedFile = new File(header[0]+".java");
			FileReader fr = new FileReader(f1);
			BufferedReader br = new BufferedReader(fr);
			int linecount=0;
			while ((line = br.readLine()) != null)
			{
				if(linecount<23)
					part1.add(line);

				else if(linecount<35)
					part2.add(line);

				else if(linecount>34)
					part3.add(line);

				linecount++;
			}
			FileWriter fw = new FileWriter(updatedFile);
			BufferedWriter out = new BufferedWriter(fw);

			part1.set(20, part1.get(20).replace("ApiTest", header[0]));
			finalList.addAll(part1);

			for (int i = 1; i < header.length; i++) {
				ArrayList<String> tempArr = new ArrayList<String>();
				tempArr.addAll(part2);

				tempArr.set(1, tempArr.get(1).replace("FuncName", header[i]));
				finalList.addAll(tempArr);
			}

			finalList.addAll(part3);

			for (Iterator iterator = finalList.iterator(); iterator.hasNext();) {
				String allLines = (String) iterator.next();
				out.append(allLines);
				out.append("\n");
			}
			out.close();
			System.out.println("Test Class Created Successfully : "+updatedFile.getAbsolutePath());
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	static public String[] getTestMethodName(String FilePath,String SheetName) {

		String[][] finalRetArray = null;
		String[][] tabArray = null;
		String[][] tempRetArray = null;
		String[] header = null;

		try {
			FileInputStream ExcelFile = new FileInputStream(FilePath);
			// Access the required test data sheet
			System.out.println("------PARSING DATA SHEET--------");
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			int startCol = 0;

			// To get the total number of columns used
			int totalCols = 0;
			Iterator rowIterator = ExcelWSheet.rowIterator();
			if (rowIterator.hasNext()) {
				Row headerRow = (Row) rowIterator.next();
				// get the number of cells in the header row

				totalCols = headerRow.getPhysicalNumberOfCells();
			}
			System.out.println("Total columns used:" + totalCols);
			header = new String[totalCols];

			//			get column Header in to array
			for (int j = startCol; j <= totalCols - 1; j++) {
				header[j] = getCellData(0, j);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

		return header;
	}

	private static String getCellData(int RowNum, int ColNum) {
		try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			int dataType = Cell.getCellType();
			if (dataType == 3) {
				return "";
			} else {
				String CellData;
				try {
					CellData = Cell.getStringCellValue();
				} catch (Exception e) {
					CellData = Cell.getRawValue();
				}
				return CellData;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
}
