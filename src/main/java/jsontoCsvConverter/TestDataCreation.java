package jsontoCsvConverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

public class TestDataCreation {


	public static String createTestDataFromJson(String filepath) {
		String[][] testData = null;
		String sheetName;
		String fileData ="";
		JSONParser parser = new JSONParser();
		try {
			FileInputStream inputStream = new FileInputStream(filepath);
			try {
				fileData = IOUtils.toString(inputStream);
			} finally {
				inputStream.close();
			}
			

			JSONObject jsonObject = new JSONObject(fileData);

			// Get item = List of folders with test data
			org.json.JSONArray msg = jsonObject.getJSONArray("item");
			// Get info = info contains the NAME of repository, Eg : FS_SearchAccount
			JSONObject obj1=(JSONObject) jsonObject.get("info");
			sheetName=obj1.getString("name");

			int arrayCounter=0;

			Map <String, ArrayList<String>> testDataMap= new HashMap<String, ArrayList<String>>();

			ArrayList<String> temp = new ArrayList<String>();
			temp.add(sheetName);
			testDataMap.put("TestSheetName",temp );

			for (int i = 0; i < msg.length(); i++) {
				ArrayList<String> testDataList = new ArrayList<String>();
				JSONObject level1=(JSONObject) msg.get(i);
				System.out.println("*************************");
				System.out.println("NAME = "+ level1.get("name"));
				org.json.JSONArray level2= level1.getJSONArray("item");

				for (int j = 0; j < level2.length(); j++) {

					JSONObject level2Data=(JSONObject) level2.get(j);

					System.out.println("NAME = "+ level2Data.get("name"));
					System.out.println("REQUEST = "+level2Data.get("request"));
					testDataList.add("NAME = "+ level2Data.get("name")+"||"+"REQUEST = "+level2Data.get("request"));
				}
				testDataMap.put(String.valueOf(level1.get("name")), testDataList);

				System.out.println("-------------------------------------------");
			}

			System.out.println(testDataMap);

			return writeToFile(testDataMap)+"|"+sheetName;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String writeToFile(Map<String, ArrayList<String>> testDataMap)
	{
		String outputFileName="Test_Data_Saorabh.xlsx";
		try {
			XSSFWorkbook myWorkBook = new XSSFWorkbook (); 
			XSSFSheet sheet = myWorkBook.createSheet(testDataMap.get("TestSheetName").get(0));
			testDataMap.remove("TestSheetName");

			int columnCount = 0;

			Set<String> temp = testDataMap.keySet();

			for (Iterator iterator = temp.iterator(); iterator.hasNext();) {
				String scenarioName = (String) iterator.next();
				columnCount++;

				//	creating Column Headers
				XSSFRow row = sheet.getRow(0);
				if (row==null) {
					row = sheet.createRow(0);
					XSSFCell cell = row.createCell(0);
					cell.setCellValue("DataSet_Name");
				}

				XSSFCell cell = row.getCell(columnCount);
				if (cell==null) 
					cell=row.createCell(columnCount);
				cell.setCellValue(scenarioName );

				int rowCount = 1;
				ArrayList<String> testcaseData = testDataMap.get(scenarioName);


				for (String celldata : testcaseData) {
					row = sheet.getRow(rowCount);
					if (row==null) {
						row = sheet.createRow(rowCount);
						XSSFCell cell1 = row.createCell(0);
						cell1.setCellValue("defaultDataSet");
					}
					cell = row.getCell(columnCount);
					if (cell==null) 
						cell=row.createCell(columnCount);
					cell.setCellValue((String) celldata);

					System.out.println("updated data for row # "+rowCount + "& column # "+columnCount+" Data= "+celldata);
					rowCount++;
				}
			}
			try (FileOutputStream outputStream = new FileOutputStream(outputFileName)) {
				myWorkBook.write(outputStream);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputFileName; 
	}



}
